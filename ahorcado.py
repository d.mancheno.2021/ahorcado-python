import random

Intentos = [11,10,9,8,7,6,5,4,3,2,1,] 

Animales = ["perro","gato","leon"]
Cuerpo = ["brazo","cabeza","rodilla"]
Paises = ["españa","francia","italia"]
Comida = ["lentejas","paella","macarrones"]
Instrumentos = ["guitarra","trompeta","saxofon"]
Trabajos = ["ingeniero","astronauta","programador"]
Deportes = ["futbol","baloncesto","tenis"]
Asignaturas = ["matematicas","informatica","biologia"]
Vehiculos = ["coche","avion","motocicleta"]
Tiempo = ["sol","lluvia","nube"]

temasMenu=["Temas Aleatorios","Animales","Cuerpo","Países","Comida","Instrumentos musicales","Trabajos","Deportes","Asignaturas escolares","Vehículos","El tiempo"]
temas=[Animales,Cuerpo,Paises,Comida,Instrumentos,Trabajos,Deportes,Asignaturas,Vehiculos,Tiempo]



def Interfaz(Intentos, letraIncorrecta, letraCorrecta, palabraSecreta):
    # Esta funcion imprime el interfaz con los intentos, las letras incorrectas, y la palabra secreta
    print("Te quedan ",Intentos[len(letraIncorrecta)]-1," intentos.\nLetras incorrectas: ",end=" ")

    for letra in letraIncorrecta:
        print (letra,end=" ")
    print()
    espacio = "_" * len(palabraSecreta)
    for i in range(len(palabraSecreta)): # Remplaza los espacios en blanco por la letra bien escrita
        if palabraSecreta[i] in letraCorrecta:
            espacio = espacio[0:i] + palabraSecreta[i] + espacio[i+1:len(espacio)]
    for letra in espacio: # Mostrara la palabra secreta con espacios entre letras
        print (letra,end=" ")
    print()
    print(len(palabraSecreta))
        
def menu(lista):
    # Esta funcion imprime el menu con los temas
    for i in range(len(lista)):
        print("[",i,"] {}".format(lista[i]))
    
def temaAleat(listaPalabras):
    # Esta funcion devuelve un tema aleatorio
    temaAleatorio = random.randint(0, len(listaPalabras) - 1)
    return temaAleatorio

def buscarPalabraAleat():
    # Esta funcion devuelve una palabra aleatoria.
    
    if opcion==0:
        temaAleatorio = temas[temaAleat(temas)]
        palabraAleatoria = random.randint(0,len(temaAleatorio) - 1)
        print(len(temaAleatorio))
        return temaAleatorio[palabraAleatoria]
    if opcion==1:
        return Animales[random.randint(0,len(Animales)-1)]
    if opcion==2:
        return Cuerpo[random.randint(0,len(Cuerpo)-1)]
    if opcion==3:
        return Paises[random.randint(0,len(Paises)-1)]
    if opcion==4:
        return Comida[random.randint(0,len(Comida)-1)]
    if opcion==5:
        return Instrumentos[random.randint(0,len(Instrumentos)-1)]
    if opcion==6:
        return Trabajos[random.randint(0,len(Trabajos)-1)]
    if opcion==7:
        return Deportes[random.randint(0,len(Deportes)-1)]
    if opcion==8:
        return Asignaturas[random.randint(0,len(Asignaturas)-1)]
    if opcion==9:
        return Vehiculos[random.randint(0,len(Vehiculos)-1)]
    if opcion==10:
        return Tiempo[random.randint(0,len(Tiempo)-1)]
    

def elijeLetra(algunaLetra):
    # Esta funcion se asegura de que la letra este en minuscula, tambien se asegura de que no se incluyen letras repetidas, mas de una letra, o caracteres que no sean letras
    while True:
        print()
        letra=input("Haz tu adivinanza: ")

        letra = letra.lower()
        if len(letra) != 1:
            print ("Introduce una sola letra.") 
        elif letra in algunaLetra:
            print ("Ya has elegido esa letra ¿Qué tal si pruebas con otra?")
        elif letra not in 'abcdefghijklmnñopqrstuvwxyz':
            print ("Elije una letra.")
        else:
            return letra
        
def empezar():
    print("Quieres jugar de nuevo? (Si o No)")
    respuesta=input().lower()
    if respuesta[0]=="s":
        return respuesta

          


nombre = input("Bienvenido al juego del ahorcado, ¿Cómo te llamas? ")
print("Hola ",nombre,", vamos a comenzar el juego!\nTemas disponibles: ")
                                                                                                    
print()

menu(temasMenu)                                                                                                  
opcion=int(input("Elige una opción: "))

print()

print("Comenzando...")
                                                                                                     
print()

letraIncorrecta = ""
letraCorrecta = ""
palabraSecreta = buscarPalabraAleat()
finJuego = False
juegosGanados=0
Juegos=0


while True:
    Interfaz(Intentos, letraIncorrecta, letraCorrecta, palabraSecreta)
    # El usuairo elije una letra.
    letra = elijeLetra(letraIncorrecta + letraCorrecta)
    if letra in palabraSecreta:
        letraCorrecta = letraCorrecta + letra
        # Se fija si el jugador ha ganado
        letrasEncontradas = True
        for i in range(len(palabraSecreta)):
            if palabraSecreta[i] not in letraCorrecta:
                letrasEncontradas = False
                break
        if letrasEncontradas:
            print ("La palabra secreta es ",palabraSecreta,"! ¡Felicidades! ¡Has ganado!")
            juegosGanados += 1
            finJuego = True
    else:
        letraIncorrecta = letraIncorrecta + letra
        print("Tu adivinanza es incorrecta!")
        # Comprueba la cantidad de letras que ha ingresado el jugador y si ha perdido
        if len(letraIncorrecta) == len(Intentos) - 1:
            Interfaz(Intentos, letraIncorrecta, letraCorrecta, palabraSecreta)
            print ("¡Se ha quedado sin letras!\nDespues de {} letras erroneas y {} letras correctas, la palabra era {}".format(len(letraIncorrecta),len(letraCorrecta),palabraSecreta))
            finJuego = True
    # Pregunta al jugador si quiere jugar de nuevo
    
    if finJuego:
        Juegos += 1
        if empezar():
            print("Llevas {} de {} ganados.".format(juegosGanados,Juegos))
            menu(temasMenu)
            opcion=int(input("Elige una opcion: "))
            letraIncorrecta = ""
            letraCorrecta = ""
            finJuego = False
            palabraSecreta = buscarPalabraAleat()
        else:
            print("Llevas {} de {} ganados. Hasta pronto!".format(juegosGanados,Juegos))
            break